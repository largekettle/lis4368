# LIS4368 - Overview

## Rafael Calderon

### Assignments

#### A1 Summary

* Install JDK
* Install Tomcat
* Provide Screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials
* Provide Git commands descriptions

>[a1 Readme.md](a1/README.md)

-------------------------------------------------------------

#### A2 Summary

* Install MYSQL
* Deploy and install Servlet
* Provide Screenshots
* Update Bitbucket repo

>[a2 Readme.md](a2/README.md)

-------------------------------------------------------------

#### A3 Summary

* Data Repository
* Provide Screenshots
* Update Bitbucket repo

>[a3 Readme.md](a3/README.md)

-------------------------------------------------------------

#### P1 Summary

* Form Validation
* Provide Screenshots
* Update Bitbucket repo

>[p1 Readme.md](p1/README.md)

-------------------------------------------------------------

#### A4 Summary

* Form Validation
* Provide Screenshots
* Update Bitbucket repo

>[a4 Readme.md](a4/README.md)

-------------------------------------------------------------

#### A5 Summary

* Basic Server-side validation
* Prepared statements to help prevent SQL injection. 
* JSTL to prevent XSS.
* Adds insert functionality to A4

>[a5 Readme.md](a5/README.md)

-------------------------------------------------------------

#### P5 Summary

* MVC framework, using basic client-server-side validation
* Prepared statements to help prevent SQL injection
* JSTL to prevent XSS. Also, completes CRUD functionality

>[P2 Readme.md](p2/README.md)

-------------------------------------------------------------