# LIS4368

## Rafael Calderon

### Assignment 1

#### Screenshots

![JDK Install](../img/jdk_install.png)

![Java Hello world](../img/hello.png)

![Tomcat](../img/tomcat.png)

#### Git definitions

* git init - This command turns a directory into an empty Git repository. This is the first step in creating a repository. After running git init, adding and committing files/directories is possible.

* git status - This command returns the current state of the repository. git status will return the current working branch. If a file is in the staging area, but not committed, it shows with git status. Or, if there are no changes it’ll return nothing to commit, working directory clean.

* git add - Adds files in the to the staging area for Git. Before a file is available to commit to a repository, the file needs to be added to the Git index (staging area). There are a few different ways to use git add, by adding entire directories, specific files, or all unstaged files.

* git commit - Record the changes made to the files to a local repository. For easy reference, each commit has a unique ID. It’s best practice to include a message with each commit explaining the changes made in a commit. Adding a commit message helps to find a particular change or understanding the changes.

* git push - Sends local commits to the remote repository. git push requires two parameters: the remote repository and the branch that the push is for.

* git pull - Gets the latest version of a repository. This pulls the changes from the remote repository to the local computer.

* git config - With Git, there are many configurations and settings possible. git config is how to assign these settings. Two important settings are user user.name and user.email. These values set what email address and name commits will be from on a local computer. With git config, a --global flag is used to write the settings to all repositories on a computer. Without a --global flag settings will only apply to the current repository that you are currently in. There are many other variables available to edit in git config. From editing color outputs to changing the behavior of git status. Learn about git config settings in the official Git documentation.

#### Links

[BitbucketStationLocations](https://bitbucket.org/largekettle/bitbucketstationlocations)

[lis4368](https://bitbucket.org/largekettle/lis4368)