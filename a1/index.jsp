<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sun, 02-19-17, 12:24:47 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Online portfolio for LIS4368.">
	<meta name="author" content="Rafael Calderon.">
	<link rel="icon" href="../favicon.ico">

	<title>LIS4368 - Assignment 1</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body class="a1">

<!-- display application path -->
<% //= request.getContextPath()%>

<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>JDK Installation:</b><br />
					<img src="../img/jdk_install.png" class="img-responsive" alt="JDK Installation" />
					<hr>
					<img src="../img/hello.png" class="img-responsive" alt="JDK Installation" />

					<br /> <br />
					<b>Tomcat Installation:</b><br />
					<img src="../img/tomcat.png" class="img-responsive" alt="Tomcat Installation" />
				</div>
			</div>
			
	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

	 <%@ include file="/js/include_js.jsp" %>	
	 <script>
			
			$( document ).ready(function() {
				$(".nav").find(".active").removeClass("active");
				var bodyClass = $('body').attr('class');
				switch (bodyClass) {
					case 'home':
						$("li.home").addClass("active");
						break;
					case 'a1':
						$("li.a1").addClass("active");
						break;
					case 'a2':
						$("li.a2").addClass("active");
						break;
					case 'a3':
						$("li.a3").addClass("active");
						break;
					case 'a4':
						$("li.a4").addClass("active");
						break;
					case 'a5':
						$("li.a5").addClass("active");
						break;
					case 'p1':
						$("li.p1").addClass("active");
						break;
					case 'p2':
						$("li.p2").addClass("active");
						break;
					case 'test':
						$("li.test").addClass("active");
						break;
				}
		
			});
		 </script>	

</body>
</html>
