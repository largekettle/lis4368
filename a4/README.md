# LIS4368

## Rafael Calderon

### Assignment 4

Basic Server-Side Validation

#### Screenshot

![A4 - Form](../img/a4-form.png "A4 - form")

![A4 - Failed Validation](../img/a4-failed.png "A4 - Failed Validation")

![A4 - Passed Validation](../img/a4-passed.png "A4 - Passed Validation")
