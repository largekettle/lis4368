# LIS4368

## Rafael Calderon

### Assignment 5

Basic Server-Side Validation

#### Screenshot

![A5 - Valid User Form Entry](../img/a5-form.png "A5 - Valid User Form Entry")

![A5 - Passed Validation](../img/a5-passed.png "A5 - Passed Validation")

![A5 - Associated Database Entry](../img/a5-db-entry.png "A5 - Associated Database Entry")
