<%@ page import="java.util.GregorianCalendar, java.util.Calendar" %>
<%@ page import="java.util.Date, java.util.TimeZone, java.text.SimpleDateFormat" %>
<%  
    GregorianCalendar currentDate = new GregorianCalendar();
    int currentYear = currentDate.get(Calendar.YEAR);
%>


<%
//https://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html
SimpleDateFormat timeFormat = new SimpleDateFormat("MMM-dd HH:mm a");
timeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
String time = timeFormat.format(new Date());
%>

<div class="footer"> &copy; Copyright <%= currentYear %> Rafael Calderon - All rights reserved. <%= time %></div>