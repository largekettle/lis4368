

<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="home"><a href="../index.jsp">LIS4368</a></li>
					<li class="a1"><a href="../a1/index.jsp">A1</a></li>
					<li class="a2"><a href="../a2/index.jsp">A2</a></li>
					<li class="a3"><a href="../a3/index.jsp">A3</a></li>
					<li class="a4"><a href="../customerform.jsp?assign_num=a4">A4</a></li>
					<li class="a5"><a href="../customerform.jsp?assign_num=a5">A5</a></li>
					<li class="p1"><a href="../p1/index.jsp">P1</a></li>
					<li class="p2"><a href="../customerform.jsp?assign_num=p2">P2</a></li>
					<li class="test"><a href="../test/index.jsp">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


