import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;

public class AsciiApp {

    public static boolean valNum(String input) {
        try { Integer.parseInt(input); }
        catch(NumberFormatException ex) { return false; }
        return true;
    }

    public static void main(String[] args) {
        char list[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        System.out.println("Printing characters A-Z as ascii values: ");
        for (int i = 0; i < 24; i++) {
            System.out.println("Character " + list[i] + " has ascii value " + (int)list[i]);
        }
        System.out.println();
        System.out.println("Printing ascii values 48-122 as characters:");
        for (int i = 48; i < 123; i++) {
            System.out.println("ASCII value " + i + " has character value " + (char)i);
        }
        
        System.out.println();
        System.out.println("Allowing user ASCII value input: ");

        Scanner scan = new Scanner(System.in);
        String choice = "0";
        boolean loop = true;
        while (loop) {
            System.out.print("Please enter ascii value (32 - 127): ");
            choice = scan.next();
            if (valNum(choice)) {
                int choiceNum = Integer.parseInt(choice);
                if (choiceNum >= 128 || choiceNum <= 31) { System.out.println("Invalid integer, ascii value must be >=32 && <=127."); }
                else { System.out.println("ASCII value " + choiceNum + " has character value " + (char)choiceNum); }
            } else {
                System.out.println("Invalid integer, ascii value must be a number.");
            }
        }
    }
}