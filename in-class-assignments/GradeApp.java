import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;


public class GradeApp {

    public static boolean isNumber(String input){

        try
        {
            Double.parseDouble(input);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }


    public static void main(String[] args) {

        ArrayList<Double> list = new ArrayList<Double>();

        System.out.println("Please enter grades that range from 0-100.");
        System.out.println("Grade average and total is rounded to two decimal places.");
        System.out.println("Program does not check for non-numeric values.");
        System.out.println("To end program, enter -1\n");

        Scanner sc = new Scanner(System.in);

        // System.out.print("Enter Grade: ");
        String choice = "0";
        DecimalFormat numberFormat = new DecimalFormat("#.0#");
        boolean loop = true;

        while (loop) {

            System.out.print("Enter Grade: ");
            choice = sc.next();

            if (isNumber(choice)) {

                double choiceNum = Double.parseDouble(choice);
                if (choiceNum >= 0 && choiceNum <= 100 && choiceNum != -1) { 

                    list.add(choiceNum); 
                } else { 

                    System.out.println("Not a valid number."); 
                }

                if (choiceNum == -1) { 

                    loop = false; 
                }

            } else {

                System.out.println("Not a valid int.");
            }
        }

        double retval = 0;

        for (int i = 0; i < list.size(); i++) {
            retval += list.get(i);
        }

        System.out.println("\nGrade Count: " + list.size());
        System.out.println("Grade Total: " + numberFormat.format(retval));
        System.out.println("Grade Average: " + numberFormat.format((retval / list.size())));

    }
}