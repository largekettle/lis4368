import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;

public class NumSwapApp {
    public static boolean isNumber(String input){
        try
        {
            Integer.parseInt(input);
            //Double.parseDouble(input);
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int num1 = 0;
        int num2 = 0;
        System.out.println("Program swaps two integers.");
        System.out.println("Checks for non-numerical values.\n");
        Scanner scan = new Scanner(System.in);
        String choice = "0";
        boolean loop = true;
        boolean err = false;
        
        while (loop) {
            if (err) { 
                System.out.print("Please try again. "); 
            }
            System.out.print("Enter first number: ");
            choice = scan.next();
            if (isNumber(choice)) {
                num1 = Integer.parseInt(choice);
                loop = false;
            } else {
                System.out.println("That's not a valid number.");
                err = true;
            }
        }
        loop = true;
        err = false;
        while (loop) {
            if (err) { System.out.print("Please try again. "); }
                System.out.print("Enter second number: ");
                choice = scan.next();
            if (isNumber(choice)) {
                num2 = Integer.parseInt(choice);
                loop = false;
            } else {
                System.out.println("That's not a valid number.");
                err = true;
            }
        }
        scan.close();
        System.out.println("Before swapping: ");
        System.out.println("Num1: " + num1);
        System.out.println("Num2: " + num2);
        System.out.println("After swapping: ");
        System.out.println("Num1: " + num2);
        System.out.println("Num2: " + num1);
    }
 }