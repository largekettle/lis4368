<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Online portfolio for LIS4368.">
	<meta name="author" content="Rafael Calderon">

	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>


		</style>

</head>

<body class="home">

	<%@ include file="/global/nav_global.jsp" %>

		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<%@ include file="/global/header.jsp" %>
				</div>
				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div id="myCarousel" class="carousel" data-interval="1000" data-pause="hover" data-wrap="true" data-keyboard="true" data-ride="carousel">

						<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>
						<!-- Carousel items -->
						<div class="carousel-inner">

							<div class="active item" style="background: url(img/slide1.jpg) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Colombia</h3>
										<p class="lead">is where colonial meets contemporary</p>
										<a class="btn btn-large btn-success" target="_blank" href="https://www.lonelyplanet.com/colombia">Learn more</a>
									</div>
								</div>
							</div>

							<div class="item" style="background: url(img/slide2.jpg) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Colombia</h3>
										<p class="lead">is a verdant wonderland</p>
										<a class="btn btn-large btn-success" target="_blank" href="https://www.lonelyplanet.com/colombia">Learn more</a>
									</div>
								</div>
							</div>

							<div class="item" style="background: url(img/slide3.jpg) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Colombia</h3>
										<p class="lead">is bright, bold avenues</p>
										<a class="btn btn-large btn-success" target="_blank" href="https://www.lonelyplanet.com/colombia">Learn more</a>
									</div>
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->

				<%@ include file="/global/footer.jsp" %>

			</div>
			<!-- end starter-template -->
		</div>
		<!-- end container -->


		<%@ include file="/js/include_js.jsp" %>

			<script>
				$(document).ready(function () {
					$(".nav").find(".active").removeClass("active");
					var bodyClass = $('body').attr('class');
					switch (bodyClass) {
						case 'home':
							$("li.home").addClass("active");
							break;
						case 'a1':
							$("li.a1").addClass("active");
							break;
						case 'a2':
							$("li.a2").addClass("active");
							break;
						case 'a3':
							$("li.a3").addClass("active");
							break;
						case 'a4':
							$("li.a4").addClass("active");
							break;
						case 'a5':
							$("li.a5").addClass("active");
							break;
						case 'p1':
							$("li.p1").addClass("active");
							break;
						case 'p2':
							$("li.p2").addClass("active");
							break;
						case 'test':
							$("li.test").addClass("active");
							break;
					}

				});
			</script>
</body>

</html>