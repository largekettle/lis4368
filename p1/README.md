# LIS4368

## Rafael Calderon

### Project 1

Basic client-side validation. This project uses client-side validation, and regular expressions.

#### Screenshots

##### Main/Splash Page

![Screenshot 1](../img/p1_screen00.png)

##### Initial Form - Customers Table

![Screenshot 2](../img/p1_screen01.png)

##### Failed Validation

![Screenshot 3](../img/p1_screen02.png)

##### Passed Validation

![Screenshot 4](../img/p1_screen03.png)