# LIS4368

## Rafael Calderon

### Project 2

MVC framework, using basic client-server-side validation. Prepared statements to help prevent SQL injection. JSTL to prevent XSS. Also, completes CRUD functionality

#### Screenshots

##### Valid User Form Entry (customerform.jsp)

![Screenshot 1](../img/p2_screen01.png)

##### Passed Validation (thanks.jsp)

![Screenshot 2](../img/p2_screen02.png)

##### Display Data (modify.jsp)

![Screenshot 3](../img/p2_screen03.png)

##### Modify Form (customer.jsp)

![Screenshot 3](../img/p2_screen04.png)

##### Modified Data (modify.jsp)

![Screenshot 3](../img/p2_screen05.png)

##### Delete Warning (modify.jsp)

![Screenshot 3](../img/p2_screen06.png)

##### Deleted Modified Data (modify.jsp)

![Screenshot 3](../img/p2_screen07.png)

##### Associated Database Changes (Select, Insert, Update, Delete)

![Screenshot 4](../img/p2_screen08.png)